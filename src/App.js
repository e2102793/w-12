
import './App.css';
import logo from './logo.jpg'

function App() {
  return (
    <div className="App">
      <div className="image">
        <img src={logo} alt="logo"></img>
        <span><b>Konepaja Stamac Oy</b></span>
        <h1>Joona Luukonen</h1>
        <span><b>CNC sorvaaja</b></span>
        <hr />
        <span className="a"><b> Yhteystiedot:</b></span>
        <br />
        <span className="b">e2102793@edu.vamk.fi</span>
        <br />
        <span className="c">0501234567</span>
        <br />
        <span className="d">Mestarintie 5, 60100 Seinäjoki</span>
      </div>
    </div>
  );
}

export default App;
